FROM elixir:1.9-alpine as BUILDER

RUN apk update
RUN apk add --update --no-cache alpine-sdk git nodejs npm make gcc libc-dev

RUN mix local.hex --force && \
  mix local.rebar --force && \
  mix hex.info

ENV MIX_ENV=prod
WORKDIR /app
ADD . .
RUN mix do deps.get, compile

# Compiling the assets
WORKDIR /app/assets
RUN npm install && \
  node node_modules/webpack/bin/webpack.js --mode production

WORKDIR /app
RUN mix phx.digest

## Create the release
WORKDIR /app
RUN mix distillery.release

FROM alpine:3.9
EXPOSE 4000
ENV REPLACE_OS_VARS=true \
  PORT=4000
RUN apk update
RUN apk add --no-cache --no-cache openssl-dev bash
WORKDIR /app
COPY --from=BUILDER /app/_build/prod/rel/tanktank_server .

CMD ["./bin/tanktank_server", "foreground"]