defmodule TanktankServerWeb.PageController do
  use TanktankServerWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
