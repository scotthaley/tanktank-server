defmodule TanktankServerWeb.MatchController do
  use TanktankServerWeb, :controller

  require Logger

  alias TanktankServer.GameState

  def create_match(conn, %{"playerName" => player_name}) do
    json(conn, %{uuid: GameState.create_match(player_name)})
  end

  def match_active(conn, %{"matchId" => match_id}) do
    case GameState.get_match(match_id) do
      %{active: true} -> json(conn, %{active: true})
      _ -> json(conn, %{active: false})
    end
  end

  def all_matches(conn, _params) do
    json(conn, %{
      matches:
        GameState.all_matches()
        |> Enum.map(fn m -> %{name: m.name, id: m.id, players: length(Map.values(m.players))} end)
    })
  end
end
