defmodule TanktankServerWeb.PlayerChannel do
  use Phoenix.Channel

  require Logger

  alias TanktankServer.{GameState, Match, Player, Bullet}

  def join("players:" <> _room_id, message, socket) do
    match_id = socket.assigns.match_id
    send(self(), {:after_join, message})
    {:ok, %{match_status: GameState.get_match(match_id)}, socket}
  end

  def handle_info({:after_join, _message}, socket) do
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id
    match = GameState.get_match(match_id)

    if match.started == false do
      {_, existing_player} = Match.get_player(match, player_name)

      if existing_player == nil do
        player = Player.create_player(player_name)

        GameState.get_match(match_id)
        |> Match.put_player_new(player)
        |> GameState.save_match(match_id)
      end
    end

    {:noreply, socket}
  end

  def handle_in("match:start", _, socket) do
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id

    GameState.get_match(match_id)
    |> Match.start_match()
    |> GameState.save_match(match_id)
  end

  def handle_in("player:input-start", payload, socket) do
    input = String.to_existing_atom(payload["type"])
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id

    match = GameState.get_match(match_id)

    case match.started do
      true ->
        match
        |> Match.get_player(player_name)
        |> Player.player_input_start(input)
        |> handle_update_response(match_id)

      false ->
        nil
    end

    {:noreply, socket}
  end

  def handle_update_response({match, player}, match_id) do
    {match, player}
    |> Match.put_player()
    |> GameState.save_match(match_id)
  end

  def handle_update_response({match, player, bullet}, match_id) do
    new_match =
      {match, player}
      |> Match.put_player()

    # can we condense this function
    {new_match, bullet}
    |> Match.add_bullet()
    |> GameState.save_match(match_id)
  end

  def handle_in("player:input-end", payload, socket) do
    input = String.to_existing_atom(payload["type"])
    player_name = socket.assigns.player_name
    match_id = socket.assigns.match_id

    GameState.get_match(match_id)
    |> Match.get_player(player_name)
    |> Player.player_input_end(input)
    |> Match.put_player()
    |> GameState.save_match(match_id)

    {:noreply, socket}
  end

  def terminate(_reason, socket) do
    # player_name = socket.assigns.player_name
    # match_id = socket.assigns.match_id

    # GameState.get_match(match_id)
    # |> Match.remove_player(player_name)
    # |> GameState.save_match(match_id)
  end

  def broadcast_match_status(match_id, match) do
    TanktankServerWeb.Endpoint.broadcast_from!(
      self(),
      "players:" <> match_id,
      "match_state",
      match
    )
  end
end
