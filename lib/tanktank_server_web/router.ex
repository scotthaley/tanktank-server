defmodule TanktankServerWeb.Router do
  use TanktankServerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TanktankServerWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", TanktankServerWeb do
    pipe_through :api

    post "/new-match", MatchController, :create_match
    post "/match-status", MatchController, :match_active
    post "/all-matches", MatchController, :all_matches
  end
end
