defmodule TanktankServer.Match do
  @derive Jason.Encoder
  defstruct(
    started: false,
    name: nil,
    id: nil,
    players: %{},
    active: nil,
    walls: [],
    game_width: 1000,
    game_height: 1000,
    grid_size: nil,
    owner: nil,
    winner: nil,
    bullets: []
  )

  @moduledoc """
  This module hosts functions for working with a match
  """

  require Logger

  alias TanktankServer.{Bullet, Player, Wall}

  @grid_size 50
  @bullet_size 5
  @tank_size 16

  @type player_list :: %{atom => Player.t()}
  @type t :: %TanktankServer.Match{
          started: boolean,
          name: charlist,
          id: <<_::288>>,
          players: player_list,
          active: boolean,
          walls: list(Wall.t()),
          game_width: number,
          game_height: number,
          grid_size: number,
          owner: charlist,
          winner: charlist,
          bullets: list(Bullet.t())
        }

  @spec new_match(<<_::28>>, charlist) :: t
  def new_match(id, player_name) do
    %TanktankServer.Match{
      started: false,
      id: id,
      name: player_name <> "'s Game",
      players: %{},
      active: true,
      grid_size: @grid_size,
      owner: player_name,
      walls: [
        Wall.new_wall(-1, 20, -1, 0),
        Wall.new_wall(-1, 20, 20, 21),
        Wall.new_wall(-1, 0, -1, 20),
        Wall.new_wall(20, 21, -1, 20),
        Wall.new_wall(5, 8, 3, 5),
        Wall.new_wall(10, 15, 10, 15),
        Wall.new_wall(3, 5, 7, 9),
        Wall.new_wall(12, 14, 3, 5),
        Wall.new_wall(2, 5, 12, 16),
        Wall.new_wall(6, 14, 7, 8),
        Wall.new_wall(17, 19, 5, 12),
        Wall.new_wall(8, 12, 16, 18)
      ],
      bullets: []
    }
  end

  @spec match_update(t) :: t
  def match_update(match) do
    case match.started do
      true -> handle_match_update(match)
      false -> match
    end
  end

  def handle_match_update(match) do
    winner = find_winner(match)

    if winner != nil do
      %{match | winner: winner, active: false}
    else
      updated_bullets = Enum.map(match.bullets, fn b -> handle_bullet_update(match, b) end)

      updated_players =
        Map.new(match.players, fn {k, v} ->
          player_bullets =
            Enum.filter(updated_bullets, fn b -> b.player_name == k and b.destroyed end)

          case check_player_bullets_collision_any(v, updated_bullets) do
            true -> {k, Player.kill_player(v)}
            false -> handle_player_update(match, k, v, length(player_bullets))
          end
        end)

      bullets_without_destroyed = Enum.filter(updated_bullets, fn b -> not b.destroyed end)

      %{match | players: updated_players, bullets: bullets_without_destroyed}
    end
  end

  def find_winner(match) do
    alive_players =
      Map.keys(match.players)
      |> Enum.filter(fn p -> not match.players[p].dead end)

    case length(alive_players) do
      1 -> List.first(alive_players)
      _ -> nil
    end
  end

  def check_player_bullets_collision_any(player, bullets) do
    Enum.any?(bullets, fn b -> check_player_bullet_collision(player, b) end)
  end

  def check_player_bullet_collision(player, bullet) do
    distance =
      :math.sqrt(
        :math.pow(player.position_x - bullet.position_x, 2) +
          :math.pow(player.position_y - bullet.position_y, 2)
      )

    distance < @tank_size + @bullet_size
  end

  @spec handle_bullet_update(t, Bullet.t()) :: Bullet.t()
  def handle_bullet_update(match, bullet) do
    new_bullet_state = Bullet.bullet_update(bullet)

    destroyed =
      Enum.any?(Map.values(match.players), fn p ->
        p.dead == false and check_player_bullet_collision(p, new_bullet_state)
      end)

    case check_collision(match, new_bullet_state, @bullet_size, bullet) do
      {false, _} -> %{new_bullet_state | destroyed: destroyed}
      {true, direction} -> Bullet.bullet_bounce(new_bullet_state, direction)
    end
  end

  @spec handle_player_update(t, charlist, Player.t(), number) :: {charlist, Player.t()}
  def handle_player_update(match, player_name, player, bullet_decrement_count) do
    new_player_state = Player.player_update(player, bullet_decrement_count)

    case check_collision(match, new_player_state, @tank_size, player) do
      {false, _} -> {player_name, new_player_state}
      {true, _} -> {player_name, player}
    end
  end

  @spec check_collision(t, %{position_x: number, position_y: number}, number, %{
          position_x: number,
          position_y: number
        }) :: boolean
  def check_collision(match, entity, hitbox_size, previous_entity) do
    wall_collide =
      Enum.find(match.walls, fn wall ->
        Wall.check_collision(wall, entity, @grid_size, hitbox_size)
      end)

    case wall_collide do
      nil ->
        {false, :na}

      _ ->
        {true, Wall.collision_direction(wall_collide, previous_entity, @grid_size, hitbox_size)}
    end
  end

  @spec players(t) :: player_list
  def players(match) do
    match.players
  end

  @spec put_player(t, Player.t()) :: t
  def put_player(match, player) do
    %{match | players: Map.put(match.players, player.name, player)}
  end

  @spec put_player({t, Player.t()}) :: t
  def put_player({match, player}) do
    %{match | players: Map.put(match.players, player.name, player)}
  end

  def put_player_new(match, player) do
    %{match | players: Map.put(match.players, player.name, position_player(match, player))}
  end

  def position_player(match, player) do
    new_player_state = %{
      player
      | position_x: Enum.random(0..1000),
        position_y: Enum.random(0..1000)
    }

    case check_collision(match, new_player_state, @tank_size, player) do
      {false, _} -> new_player_state
      {true, _} -> position_player(match, player)
    end
  end

  @spec add_bullet({t, Bullet.t()}) :: t
  def add_bullet({match, bullet}) do
    %{match | bullets: match.bullets ++ [bullet]}
  end

  @spec remove_player(t, charlist) :: t
  def remove_player(match, player_name) do
    %{match | players: Map.drop(match.players, [player_name])}
  end

  @spec get_player(t, charlist) :: {t, Player.t()}
  def get_player(match, player_name) do
    {match, match.players[player_name]}
  end

  def start_match(match) do
    %{match | started: true}
  end
end
