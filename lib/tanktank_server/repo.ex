defmodule TanktankServer.Repo do
  use Ecto.Repo,
    otp_app: :tanktank_server,
    adapter: Ecto.Adapters.Postgres
end
