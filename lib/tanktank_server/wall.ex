defmodule TanktankServer.Wall do
  @derive Jason.Encoder
  defstruct(
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  )

  @type t :: %TanktankServer.Wall{
          top: number,
          bottom: number,
          left: number,
          right: number
        }

  @spec new_wall(number, number, number, number) :: t
  def new_wall(top, bottom, left, right) do
    %TanktankServer.Wall{
      top: top,
      bottom: bottom,
      left: left,
      right: right
    }
  end

  @spec check_collision(t, %{position_x: number, position_y: number}, number, number) :: boolean
  def check_collision(wall, entity, grid_size, hitbox_size) do
    t = wall.top * grid_size
    b = wall.bottom * grid_size
    l = wall.left * grid_size
    r = wall.right * grid_size
    x = entity.position_x
    y = entity.position_y

    y + hitbox_size > t and y - hitbox_size < b and x + hitbox_size > l and x - hitbox_size < r
  end

  @spec check_collision(t, %{position_x: number, position_y: number}, number, number) :: atom
  def collision_direction(wall, entity, grid_size, hitbox_size) do
    t = wall.top * grid_size
    b = wall.bottom * grid_size
    l = wall.left * grid_size
    r = wall.right * grid_size
    x = entity.position_x
    y = entity.position_y

    case {x, y} do
      {x, _} when x < l -> :l
      {x, _} when x > r -> :r
      {_, y} when y < t -> :t
      _ -> :b
    end
  end
end
