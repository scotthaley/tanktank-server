defmodule TanktankServer.GameState do
  @moduledoc """
  This module holds the state for all current games.
  """

  require Logger

  alias TanktankServer.Match
  alias TanktankServerWeb.PlayerChannel

  @doc """
  Used by the supervisor to start the Agent that will keep game state persistent.
  Initial value passed to the Agent is an empty map that will hold all games by ID.
  """
  def start_link do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  def tick_all_matches() do
    matches = Agent.get(__MODULE__, fn matches -> matches end)

    Enum.each(matches, fn {k, v} ->
      match_tick(v, k)
    end)
  end

  def match_tick(match, match_id) do
    PlayerChannel.broadcast_match_status(match_id, match)

    Match.match_update(match)
    |> save_match(match_id)
  end

  def all_matches() do
    matches = Agent.get(__MODULE__, fn matches -> matches end)

    Map.values(matches)
    |> Enum.filter(fn m -> m.active end)
  end

  @spec create_match(charlist) :: <<_::288>>
  def create_match(player_name) do
    uuid = Ecto.UUID.generate()
    Agent.update(__MODULE__, &Map.put(&1, uuid, Match.new_match(uuid, player_name)))
    uuid
  end

  @spec get_match(<<_::288>>) :: Match.t()
  def get_match(match_id) do
    Agent.get(__MODULE__, &get_in(&1, [match_id]))
  end

  @spec save_match({Match.t(), any}, <<_::288>>) :: any
  def save_match({new_state, _other}, match_id) do
    Agent.update(__MODULE__, &put_in(&1, [match_id], new_state))
    new_state
  end

  @spec save_match(Match.t(), <<_::288>>) :: any
  def save_match(new_state, match_id) do
    Agent.update(__MODULE__, &put_in(&1, [match_id], new_state))
    new_state
  end
end
