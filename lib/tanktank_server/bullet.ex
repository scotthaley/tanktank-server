defmodule TanktankServer.Bullet do
  @derive Jason.Encoder
  defstruct [
    :player_name,
    :position_x,
    :position_y,
    :rot,
    :bounces,
    :destroyed
  ]

  require Math
  @bullet_speed 20

  @type t :: %TanktankServer.Bullet{
          player_name: charlist,
          position_x: float,
          position_y: float,
          rot: integer,
          bounces: integer,
          destroyed: boolean
        }

  def create_bullet(player) do
    rotation = player.rot * Math.pi() / 180

    %TanktankServer.Bullet{
      player_name: player.name,
      position_x: player.position_x + Math.cos(rotation) * 20,
      position_y: player.position_y + Math.sin(rotation) * 20,
      rot: player.rot,
      bounces: 0,
      destroyed: false
    }
  end

  @spec bullet_bounce(t, atom) :: t
  def bullet_bounce(bullet, direction) do
    case {bullet.bounces, direction} do
      {1, _} -> %{bullet | destroyed: true}
      {_, :l} -> %{bullet | bounces: bullet.bounces + 1, rot: 180 - bullet.rot}
      {_, :r} -> %{bullet | bounces: bullet.bounces + 1, rot: 180 - bullet.rot}
      {_, :t} -> %{bullet | bounces: bullet.bounces + 1, rot: 360 - bullet.rot}
      {_, :b} -> %{bullet | bounces: bullet.bounces + 1, rot: 360 - bullet.rot}
    end
  end

  @spec bullet_update(t) :: t
  def bullet_update(bullet) do
    %{
      bullet
      | position_x: position_x_update(bullet.position_x, bullet.rot),
        position_y: position_y_update(bullet.position_y, bullet.rot)
    }
  end

  @spec position_x_update(float, integer) :: float
  def position_x_update(pos_x, rot) do
    pos_x + Math.cos(rot * Math.pi() / 180) * @bullet_speed
  end

  @spec position_y_update(float, integer) :: float
  def position_y_update(pos_y, rot) do
    pos_y + Math.sin(rot * Math.pi() / 180) * @bullet_speed
  end
end
