defmodule TanktankServer.Player do
  @derive Jason.Encoder
  defstruct [
    :name,
    :position_x,
    :position_y,
    :rot,
    :turning,
    :gas,
    :dead,
    :bullet_count
  ]

  require Math

  alias TanktankServer.Bullet
  @rot_speed 10
  @tank_speed 5

  @type t :: %TanktankServer.Player{
          name: charlist,
          position_x: float,
          position_y: float,
          rot: integer,
          turning: integer,
          gas: integer,
          dead: boolean,
          bullet_count: integer
        }

  @spec create_player(charlist) :: t
  def create_player(name) do
    %TanktankServer.Player{
      name: name,
      position_x: 50.0,
      position_y: 50.0,
      rot: 0,
      turning: 0,
      gas: 0,
      dead: false,
      bullet_count: 0
    }
  end

  def kill_player(player) do
    %{
      player
      | dead: true
    }
  end

  @spec player_update(t, number) :: t
  def player_update(player, bullets_to_decrement) do
    case player.dead do
      false ->
        %{
          player
          | rot: rot_update(player.rot, player.turning),
            position_x: position_x_update(player.position_x, player.rot, player.gas),
            position_y: position_y_update(player.position_y, player.rot, player.gas),
            bullet_count: max(player.bullet_count - bullets_to_decrement, 0)
        }

      true ->
        player
    end
  end

  @spec rot_update(integer, integer) :: integer
  def rot_update(rot, change) do
    new_rot = rot + change * @rot_speed

    case new_rot do
      n when n >= 0 ->
        rem(new_rot, 360)

      _ ->
        360 + new_rot
    end
  end

  @spec position_x_update(float, integer, integer) :: float
  def position_x_update(pos_x, rot, change) do
    pos_x + Math.cos(rot * Math.pi() / 180) * change * @tank_speed
  end

  @spec position_y_update(float, integer, integer) :: float
  def position_y_update(pos_y, rot, change) do
    pos_y + Math.sin(rot * Math.pi() / 180) * change * @tank_speed
  end

  @spec player_input_start({Match.t(), t}, atom) :: {Match.t(), t}
  def player_input_start({match, player}, :left) do
    {match, %{player | turning: -1}}
  end

  def player_input_start({match, player}, :right) do
    {match, %{player | turning: 1}}
  end

  def player_input_start({match, player}, :forward) do
    {match, %{player | gas: 1}}
  end

  def player_input_start({match, player}, :back) do
    {match, %{player | gas: -0.5}}
  end

  def player_input_start(
        {match, %TanktankServer.Player{bullet_count: bullet_count} = player},
        :shoot
      )
      when bullet_count < 3 do
    case player.dead do
      false ->
        {match, %{player | bullet_count: bullet_count + 1}, Bullet.create_bullet(player)}

      true ->
        {match, player}
    end
  end

  def player_input_start({match, player}, :shoot) do
    {match, player}
  end

  @spec player_input_end({Match.t(), t}, atom) :: {Match.t(), t}
  def player_input_end({match, player}, :turn) do
    {match, %{player | turning: 0}}
  end

  def player_input_end({match, player}, :gas) do
    {match, %{player | gas: 0}}
  end
end
