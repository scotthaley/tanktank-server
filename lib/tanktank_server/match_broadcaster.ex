defmodule TanktankServer.MatchBroadcaster do
  use GenServer
  require Logger

  alias TanktankServer.GameState

  @gametick 50

  def start_link(_default) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def init(state) do
    Process.send_after(self(), :tick, @gametick)
    {:ok, state}
  end

  def handle_info(:tick, state) do
    GameState.tick_all_matches()
    Process.send_after(self(), :tick, @gametick)
    {:noreply, state}
  end
end
